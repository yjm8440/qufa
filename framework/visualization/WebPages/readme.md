# QuFa Visualization

1. Raw Data (.csv) 의 컬럼 지정 후 파일 Loading 및 parsing
2. 로딩된 Dataset 을 OverView 및 Facets Dive 로 가시화

## Dataset
* 시카고시에서 발생한 교통사고 데이터 : 201008_origin_123605402.csv (부산대 제공)

## Development

### 웹 폴더 및 파일 구조
* /root
    + /css
        - qupa.css
    + /js
        - facets.html
        - facets.html.js
        - qupa.js
        - worker.js
    - index.html

### 개발 환경
* Browser :
    - Google Chrome v86.0
* Language :
    - HTML
    - JAVA SCRIPT
* Library : 
    - JQuery
    - Bootstrap
    - FontAwesome
    - ChartJS
    - D3
    - Google Facets

## Comment
* 현재 프로젝트 파일 다운로드 후 진입 파일(index.html)로 웹브라우저(Chrome에서만 동작)에서 실행 가능